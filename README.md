source: http://www.w3sdesign.com/

The intent of the Strategy design pattern is to:
"Define a family of algorithms, encapsulate each one, and make them interchange­able. Strategy lets the algorithm vary independently from clients that use it." [GoF]

The Strategy design pattern solves problems like:
How can a class be configured with an algorithm at run-time instead of implementing an algorithm directly?
How can an algorithm be selected and exchanged at run-time?

The Strategy pattern describes how to solve such problems:
Define a family of algorithms, encapsulate each one, - define separate classes (Strategy1,Strategy2,…) that implement (encapsulate) each algorithm,
and make them interchangeable - and define a common interface (Strategy) through which algorithms can be (inter)changed at run-time.

The Strategy design pattern provides a solution:
Encapsulate an algorithm in a separate Strategy object.
 A class delegates an algorithm to a Strategy object instead of implementing an algorithm directly