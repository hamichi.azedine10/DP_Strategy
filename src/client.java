public class client {
    // cette classe doit utiliser un certain nombre d'algorithmes interchangeable dans le temps (rutime)
   Istartegy istartegy;

    public client(Istartegy istartegy) {
        this.istartegy = istartegy;
    }

    public Istartegy getIstartegy() {
        return istartegy;
    }

    public void setIstartegy(Istartegy istartegy) {
        this.istartegy = istartegy;
    }

    // autre code ..
    public void  effectuerOperation(){
        istartegy.appliquerStrategy();
    };
   // autre code ...
}


